{
    'name': '发票导出为xml',
    'version': '13.0.0.1',
    'author': "993832367@qq.com",
    'summary': '发票导出为xml',
    'category': 'gooderp',
    'description':
    '''
    发票导出为xml
    ''',
    'data': [
     'views/tax_invoice_to_xml.xml'
     ],
    'depends': ['tax_invoice', 'scm'],
}