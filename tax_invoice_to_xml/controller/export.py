from odoo import http
from odoo.http import request, content_disposition
from odoo.addons.web.controllers.main import serialize_exception


class Exportxml(http.Controller):
    @http.route('/exports', type='http', auth='user')
    @serialize_exception
    def export_xml(self, result_data, file_name):
        return request.make_response(result_data, headers=[('Content-Disposition', content_disposition(file_name)),
                                                           ('Content-Type', 'application/xml')])
