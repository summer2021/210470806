from odoo import fields, models, api
from xml.dom.minidom import Document, parse
import os

# 销货发票确认状态可选值
TAX_INVOICE_TO_XML_STATES = [
    ('draft', '未申请开票'),
    ('submit', '已申请开票'),
    ('done', '已完成')]

# 字段只读状态
READONLY_STATES = {
    'submit': [('readonly', True)],
    'done': [('readonly', True)],
}


class TaxInvoiceToXmlButton(models.TransientModel):
    _name = "tax_invoice_to_xml_button"
    _description = '导出金税xml'

    def set_default_note(self):
        """
        设置默认值, 用来确认需要批量导出的发票
        """
        context = self.env.context
        order_names = [order.name for order in self.env['tax.invoice'].browse(context.get('active_ids'))]
        return '-'.join(order_names)

    note = fields.Char('本次导出销售发票', default=set_default_note, readonly=True)

    @api.model
    def file_content(self):
        context = self.env.context
        invoices = self.env['tax.invoice'].browse(context.get('active_ids'))
        f = '<Zsl>%s</Zsl><Fpsj>' % len(invoices)                      #此文件含有单据数量
        for inv in invoices:
            f += '<Fp>'
            f += '<Djh>%s</Djh>' % inv.order_id                        #单据号（20字节）
            f += '<Gfmc>%s</Gfmc>' % inv.partner_id.name               #购方名称（100字节）
            f += '<Gfsh>%s</Gfsh>' % inv.partner_code or ''            #购方税号
            f += '<Gfyhzh>%s</Gfyhzh>' % inv.partner_bank_number       #购方银行账号（100字节）
            f += '<Gfdzdh>%s</Gfdzdh>' % inv.partner_address or ''     #购方地址电话（100字节）
            f += '<Bz>%s</Bz>' % inv.note or ''                        #备注（240字节）
            f += '<Fhr>fhr</Fhr>'                                      #复核人（8字节）
            f += '<Skr>skr</Skr>'                                      #收款人（8字节）
            f += '<Spbmbbh>13.0</Spbmbbh>'                             #商品编码版本号（20字节）（必输项）
            f += '<Hsbz>0</Hsbz>'                                      #含税标志0：不含税税率，1：含税税率，2：差额税;中外合作油气田（原海洋石油）5%税率、1.5%税率为1，差额税为2，其他为0

            Xh = 0
            for line in inv.line_ids:
                Xh += 1
                f += '<Spxx>'
                f += '<Sph>'
                f += '<Xh>%d</Xh>' % Xh                                #序号
                f += '<Spmc>%s</Spmc>' % line.goods_name               #商品名称
                f += '<Ggxh>%s</Ggxh>' % line.partnumber or ''         #规格型号（40字节）
                # f += '<Jldw>%s</Jldw>' % line.uom                      #计量单位（32字节）
                f += '<Spbm>%s</Spbm>' % line.tax_catagory             #商品编码（19字节）长度必须一致（必输项）
                f += '<Qyspbm>%s</Qyspbm>' % line.goods_id.code        #企业商品编码（20字节）
                f += '<Syyhzcbz>0</Syyhzcbz>'
                f += '<Lslbz></Lslbz>'
                f += '<Yhzcsm></Yhzcsm>'
                f += '<Dj>%s</Dj>' % line.price                        #单价
                f += '<Sl>%s</Sl>' % line.quantity                     #数量
                f += '<Je>%s</Je>' % line.amount                       #金额，当金额为复数时为折扣行
                f += '<Slv>%s</Slv>' % (line.tax_rate/100)             #税率
                f += '<Kce>0</Kce>'                                    #扣除额，用于差额税计算
                f += '</Sph>'
                f += '</Spxx>'
            f += '</Fp>'
        return f

    def to_xml_button(self):
        data = '<?xml version="1.0" encoding="GBK" ?><kp><Version>2.0</Version><Fpxx>'
        data += self.file_content()
        data += '</Fpsj></Fpxx></kp>'
        result_data = data

        return {
            'type': 'ir.actions.act_url',
            'url': '/exports?file_name=term.xml&result_data=%s' % result_data,
            'target': 'new',
        }
